using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{
    enum Personagem {Professora, Aluno1, Aluno2, Aluno3, Final}
    //public TextMeshProUGUI dialogueText;
    public Text dialogueText;
    [TextArea]
    public string[] sentences;
    public int index=0;
    public float dialogue_speed;
    public float normal_speed;
    public bool nextText = true;
    public GameObject persnagemUsando;
    public GameObject mapa;
    public bool isAuto = false;
    public GameObject chatBox;
    [TextArea] public string posGame;
    public bool passouODialogo;
    //public GameObject shopBox;
    [Header("Personagem Falando")] [SerializeField] Personagem persona;
    public GameObject quiz, trunfo, anagrama;
    public GameManager gm;
    public GameObject local_de_fala;
    public string nome_do_jogador = "Jogador";
    public string nome_do_personagem;
    public List<Sprite> emocoes;
    public Color enable, disable;
    public GameObject posJogador;
    public GameObject setaDesafiante, setaJogador;
    
    private int DialogoProf;
    //Verify if the player is closer to the NPC
    

    void Start()
    {
        normal_speed = dialogue_speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=false;
            //shopButton.SetActive(false);
            //chatBox.SetActive(false);
            //shopBox.SetActive(false);
        }
    }

    //NPC Chatbox

    void NextSentence()
    {
        if(index <= sentences.Length - 1)
        {
            CadaDialogo();
        }
        else
        {
            IdentifyPersonagem();
            Auto();
            passouODialogo = true;
        }
    }

    IEnumerator WriteSentence()
    {
        foreach(char character in sentences[index].ToCharArray())
        {
            dialogueText.text += character;
            yield return new WaitForSeconds(dialogue_speed);
        }
        index++;
        nextText=true;
        if (isAuto)
        {
            ChatBox();
        }
    }

    void Final()
    {
        switch (persona)
        {
            case Personagem.Professora:
                gameObject.SetActive(false);
                break;
            case Personagem.Aluno1:
                SceneManager.LoadScene("quiz teste");
                persnagemUsando.SetActive(false);
                break;
            case Personagem.Aluno2:
                SceneManager.LoadScene("trunfo teste");
                persnagemUsando.SetActive(false);
                break;
            case Personagem.Aluno3:
                SceneManager.LoadScene("anagramateste");
                persnagemUsando.SetActive(false);
                break;
        }
    }
    
    IEnumerator WriteSentencFinal()
    {
        foreach(char character in posGame.ToCharArray())
        {
            dialogueText.text += character;
            yield return new WaitForSeconds(dialogue_speed);
        }

        nextText = true;
    }

    public void ChatBox()
    {
        ComecaDialogo();
        if (passouODialogo)
        {
            Debug.Log("passou");
            switch (persona)
            {
                case Personagem.Professora:
                    dialogueText.text = "";
                    StartCoroutine(WriteSentencFinal());
                    nextText = false;
                    break;
                case Personagem.Aluno1:
                    dialogueText.text = "";
                    StartCoroutine(WriteSentencFinal());
                    nextText = false;
                    break;
                case Personagem.Aluno2:
                    dialogueText.text = "";
                    StartCoroutine(WriteSentencFinal());
                    nextText = false;
                    break;
                case Personagem.Aluno3:
                    dialogueText.text = "";
                    StartCoroutine(WriteSentencFinal());
                    nextText = false;
                    break;
            }
            chatBox.SetActive(true);
        }
        else
        {
            chatBox.SetActive(true);

            if (nextText)
            {
                NextSentence();
                nextText = false;
            }
            
        }
    }

    public void ResetIndex()
    {
        if (!passouODialogo)
        {
            index = 0;
            nextText = true;
        }
    }

    public void Instantantio()
    {
        dialogue_speed -= dialogue_speed;
    }

    public void NormalSpeed()
    {
        dialogue_speed = normal_speed;
    }

    void IdentifyPersonagem()
    {
        switch (persona)
        {
            case Personagem.Professora:
                gm.startGame = true;
                PlayerPrefs.SetInt("StartGame", 1);
                PlayerPrefs.SetInt("DialogoProfessora", 1);
                PlayerPrefs.Save();
                gameObject.SetActive(false);
                break;
            case Personagem.Aluno1:
                if (!gm.desafio1)
                {
                    /*mapa.SetActive(false);
                    persnagemUsando.SetActive(false);
                    quiz.SetActive(true);
                    quiz.GetComponent<QuizManager>().StartTheQuiz();*/
                    SceneManager.LoadScene("quiz teste");
                }
                persnagemUsando.SetActive(false);
                break;
            case Personagem.Aluno2:
                if (!gm.desafio2)
                {
                    /*mapa.SetActive(false);
                    persnagemUsando.SetActive(false);
                    trunfo.SetActive(true);
                    trunfo.GetComponent<TrunfoManager>().StartTheTrunfo();*/
                    SceneManager.LoadScene("trunfo teste");
                }
                persnagemUsando.SetActive(false);
                break;
            case Personagem.Aluno3:
                if (!gm.desafio3)
                {
                    /*mapa.SetActive(false);
                    persnagemUsando.SetActive(false);
                    anagrama.SetActive(true);*/
                    SceneManager.LoadScene("anagramateste");
                }
                persnagemUsando.SetActive(false);
                //anagrama.GetComponent<Anagram>().StartTheTrunfo();
                break;
            case Personagem.Final:
                SceneManager.LoadScene(0);
                break;
        }
    }

    void CadaDialogo()
    {
        switch (persona)
        {
            case Personagem.Professora:
                switch (index)
                {
                    case 1:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        setaJogador.SetActive(true);
                        setaDesafiante.SetActive(false);
                        setaJogador.SetActive(true);
                        setaDesafiante.SetActive(false);
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    case 5:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        setaJogador.SetActive(true);
                        setaDesafiante.SetActive(false);
                        posJogador.GetComponent<LocalJogador>().SetConfuso();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    case 6:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[1];
                        break;
                    case 7:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        setaJogador.SetActive(true);
                        setaDesafiante.SetActive(false);
                        posJogador.GetComponent<LocalJogador>().SetTriste();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    default:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[3];
                        break;
                }
                break;
            case Personagem.Aluno1:
                switch (index)
                {
                    case 0:
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[0];
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        break;
                    case 1:
                        if (persona == Personagem.Aluno1)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            setaJogador.SetActive(true);
                            setaDesafiante.SetActive(false);
                            posJogador.GetComponent<LocalJogador>().SetConvencido();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 2:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[1];
                        break;
                    case 3:
                        if (persona == Personagem.Aluno1)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            setaJogador.SetActive(true);
                            setaDesafiante.SetActive(false);
                            posJogador.GetComponent<LocalJogador>().SetConfuso();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 4:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[0];
                        break;
                    case 6:
                        if (persona == Personagem.Aluno1)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            setaJogador.SetActive(true);
                            setaDesafiante.SetActive(false);
                            posJogador.GetComponent<LocalJogador>().SetConvencido();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 7:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[1];
                        break;
                    default:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        setaJogador.SetActive(false);
                        setaDesafiante.SetActive(true);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[3];
                        break;
                }
                break;
            case Personagem.Aluno2:
                switch (index)
                {
                    case 1:
                        if (persona == Personagem.Aluno2)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            posJogador.GetComponent<LocalJogador>().SetConfuso();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 3:
                        if (persona == Personagem.Aluno2)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            posJogador.GetComponent<LocalJogador>().SetNormal();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 4:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[4];
                        break;
                    case 8:
                        if (persona == Personagem.Aluno2)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            posJogador.GetComponent<LocalJogador>().SetConvencido();
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    default:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[3];
                        break;
                } 
                break;
            case Personagem.Aluno3:
                switch (index)
                {
                    case 1:
                        if (persona == Personagem.Aluno3)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 2:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[0];
                        break;
                    case 3:
                        if (persona == Personagem.Aluno3)
                        {
                            nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                            persnagemUsando.GetComponent<Image>().color = disable;
                        }
                        break;
                    case 4:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[0];
                        break;
                    case 5:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        posJogador.GetComponent<LocalJogador>().SetConfuso();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    case 10:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                            local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                            posJogador.GetComponent<LocalJogador>().SetColor(1);
                        posJogador.GetComponent<LocalJogador>().SetConfuso();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    default:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[3];
                        break;
                } 
                break;
            
            case Personagem.Final:
                switch (index)
                {
                    case 1:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        posJogador.GetComponent<LocalJogador>().SetConvencido();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    case 4:
                        nome_do_jogador = PlayerPrefs.GetString("Nome");
                        local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                        posJogador.GetComponent<LocalJogador>().SetColor(1);
                        posJogador.GetComponent<LocalJogador>().SetVitoria();
                        persnagemUsando.GetComponent<Image>().color = disable;
                        break;
                    default:
                        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                        persnagemUsando.GetComponent<Image>().color = enable;
                        posJogador.GetComponent<LocalJogador>().SetColor(0);
                        persnagemUsando.GetComponent<Image>().sprite = emocoes[3];
                        break;
                }
                break;
        }
        /*switch (index)
        {
            case 1:
                if (persona == Personagem.Professora)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno1)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno2)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno3)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 3:
                if (persona == Personagem.Aluno1)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno2)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno3)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 5:
                if (persona == Personagem.Professora)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                if (persona == Personagem.Aluno3)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 6:
                if (persona == Personagem.Aluno1)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 7:
                if (persona == Personagem.Professora)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 8:
                if (persona == Personagem.Aluno2)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            case 10:
                if (persona == Personagem.Aluno3)
                {
                    nome_do_jogador = PlayerPrefs.GetString("Nome");
                    local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                }
                break;
            default:
                local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                break;
        }*/
        dialogueText.text = "";
        StartCoroutine(WriteSentence());
    }

    public void Skip()
    {
        index = sentences.Length;
        IdentifyPersonagem();
        passouODialogo = true;
    }
    
    public void Auto()
    {
        isAuto = !isAuto;
        Instantantio();
    }

    public void ComecaDialogo()
    {
        switch (persona)
        {
            case Personagem.Professora:
                if (gm.startGame) passouODialogo = true;
                break;
            case Personagem.Aluno1:
                if (gm.desafio1) passouODialogo = true;
                break;
            case Personagem.Aluno2:
                if (gm.desafio2) passouODialogo = true;
                break;
            case Personagem.Aluno3:
                if (gm.desafio3) passouODialogo = true;
                break;
            case Personagem.Final:
                break;
        }
    }

    public virtual void Seta()
    {
        if (!nextText)
        {
            //stopLetering = true;
            dialogue_speed = 0;
            return;
        }
        else if (nextText)
        {
            if(passouODialogo) Final();
            else
            {
            NormalSpeed();
            ChatBox();
            return;
            }
        }
    }

    public void GotoMenu()
    {
        ResetIndex();
        SceneManager.LoadScene(0);
    }

}
