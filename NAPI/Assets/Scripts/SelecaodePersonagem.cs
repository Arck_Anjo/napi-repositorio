using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelecaodePersonagem : MonoBehaviour
{

    public List<Sprite> spriteJogador;
    public GameObject posPrincipal;
    public GameObject esqPos, dirPos;
    public int index, indexAnt, indexPost;
    
    // Update is called once per frame
    void Update()
    {
        indexAnt = index - 1;
        if (indexAnt < 0)
        {
            indexAnt = spriteJogador.Count - 1;
        }

        indexPost = index + 1;
        if (indexPost >= spriteJogador.Count)
        {
            indexPost = 0;
        }


        posPrincipal.GetComponent<Image>().sprite = spriteJogador[index];
        esqPos.GetComponent<Image>().sprite = spriteJogador[indexAnt];
        dirPos.GetComponent<Image>().sprite = spriteJogador[indexPost];

    }


    public void Plus()
    {
        index++;
        if (index >= spriteJogador.Count)
        {
            index = 0;
        }
    }

    public void Minus()
    {
        index--;
        if (index <= -1)
        {
            index = spriteJogador.Count-1;
        }
    }


    public void SetPlayer()
    {
        PlayerPrefs.SetInt("PlayerAP", index);
        PlayerPrefs.Save();
    }
    
    
}
