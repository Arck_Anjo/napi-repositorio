using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalJogador : MonoBehaviour
{
    public List<Sprite> spriteJogador;
    public List<Sprite> spriteConfuso, spriteConvencido, spriteDerrota, spriteTriste, spriteVitoria;
    public GameObject posPrincipal;
    public int index, indexAnt, indexPost;
    public Color desable, enable;
    
    // Start is called before the first frame update
    void Start()
    {
        index = PlayerPrefs.GetInt("PlayerAP"); 
        posPrincipal.GetComponent<Image>().sprite = spriteJogador[index];
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void SetColor(int valor)
    {
        if (valor == 1)
        {
            posPrincipal.GetComponent<Image>().color = enable;
        }
        else
        {
            
            posPrincipal.GetComponent<Image>().color = desable;
        }
    }

    public void SetConfuso()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteConfuso[index];
    }

    public void SetConvencido()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteConvencido[index];
    }

    public void SetDerrota()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteDerrota[index];
    }

    public void SetTriste()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteTriste[index];
    }
    
    public void SetVitoria()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteVitoria[index];
    }

    public void SetNormal()
    {
        posPrincipal.GetComponent<Image>().sprite = spriteJogador[index];
    }
}
