using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TrunfoManager : MonoBehaviour
{
    
    public List<GameObject> quiz;
    public int contagem = 0;
    private int acertos;
    public GameManager gm;
    public GameObject mapaMundi;
    [TextArea]
    public string[] falasVitoria;
    [TextArea]
    public string[] falasDerrota;
    public Text dialogueText;
    public int escolha;
    public float dialogue_speed;
    public float normal_speed;
    public GameObject desafioanteGame, desafiantePosGame;
    public Sprite win, lose;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {
        if (acertos >= ((int)quiz.Count/2))
        {
            //gm.desafio2 = true;
            PlayerPrefs.SetInt("Desafio2Complete", 1);
        }
    }
    public void StartTheTrunfo()
    {
        quiz[contagem].SetActive(true);
    }

    public void NextQuestion()
    {
        quiz[contagem].SetActive(false);
        contagem++;
        if (quiz.Count > contagem)
        {
            quiz[contagem].SetActive(true);
        }
        else
        {
            Resultado();
        }
    }

    public void Certo()
    {
        //gm.pontos++;
        acertos++;
        player.GetComponent<LocalJogador>().SetVitoria();
    }

    public void Errad()
    {
        //gm.pontos--;
        player.GetComponent<LocalJogador>().SetDerrota();
    }
    
    void Resultado()
    {
        if (acertos >= (quiz.Count / 2))
        {
            Vitoria();
        }
        else
        {
            Derrota();
        }
    }

    void Vitoria()
    {
        desafioanteGame.SetActive(false);
        desafiantePosGame.SetActive(true);
        dialogueText.text = "";
        escolha = Random.Range(0, falasVitoria.Length);
        mapaMundi.GetComponent<Image>().sprite = lose;
        StartCoroutine(WriteSentence(falasVitoria[escolha]));
        
    }

    void Derrota()
    {
        desafioanteGame.SetActive(false);
        desafiantePosGame.SetActive(true);
        dialogueText.text = "";
        escolha = Random.Range(0, falasVitoria.Length);
        mapaMundi.GetComponent<Image>().sprite = win;
        StartCoroutine(WriteSentence(falasDerrota[escolha]));
        contagem = 0;
        acertos = 0;
    }
    
    IEnumerator WriteSentence(string resultado)
    {
        foreach(char character in resultado.ToCharArray())
        {
            dialogueText.text += character;
            yield return new WaitForSeconds(dialogue_speed);
        }
        
        yield return new WaitForSeconds(3f);
        End();
        //index++;
        //nextText=true;
    }

    public void End()
    {
        /*mapaMundi.SetActive(true);
        gameObject.SetActive(false);*/
        SceneManager.LoadScene(1);
    }
    
    public void Instantantio()
    {
        dialogue_speed -= dialogue_speed;
    }

    public void NormalSpeed()
    {
        dialogue_speed = normal_speed;
    }

    public void Skip()
    {
        End();
    }
}
