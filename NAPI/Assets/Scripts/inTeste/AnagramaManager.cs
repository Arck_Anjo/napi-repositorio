using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnagramaManager : MonoBehaviour
{
    //public TextMeshProUGUI dialogueText;
    public Text dialogueText;
    [TextArea]
    public string[] sentences;
    public int index=0;
    public float dialogue_speed;
    public float normal_speed;
    public bool nextText = true;

    //public bool isTrigger=false;
    public GameObject chatBox; 
    public GameObject textos;
    public GameObject games;

    //Verify if the player is closer to the NPC

    void Start()
    {
        normal_speed = dialogue_speed;
        ChatBox();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=false;
            //shopButton.SetActive(false);
            //chatBox.SetActive(false);
            //shopBox.SetActive(false);
        }
    }

    //NPC Chatbox

    void NextSentence()
    {
        if(index <= sentences.Length - 1)
        {
            dialogueText.text = "";
            StartCoroutine(WriteSentence());
        }
        else
        {
            textos.SetActive(false);
            games.SetActive(true);
        }
    }

    IEnumerator WriteSentence()
    {
        foreach(char character in sentences[index].ToCharArray())
        {
            dialogueText.text += character;
            yield return new WaitForSeconds(dialogue_speed);
        }
        index++;
        nextText=true;
    }

    public void ChatBox()
    {
        chatBox.SetActive(true);
        if(nextText)
        {
            NextSentence();
            nextText=false;
        }
    }

    public void ResetIndex()
    {
        index = 0;
        nextText=true;
    }

    public void Instantantio()
    {
        dialogue_speed -= dialogue_speed;
    }

    public void NormalSpeed()
    {
        dialogue_speed = normal_speed;
    }
}
