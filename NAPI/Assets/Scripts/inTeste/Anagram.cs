using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class Anagram : MonoBehaviour
{
    public string palavra;
    public List<Text> txt;
    public List<string> letras;
    public List<string> certo;
    public GameObject localTxt;
    private int pos = 0;
    public int acertos;
    public int posInGame;
    //places
    public List<GameObject> places;
    public GameManager gm;
    public GameObject mapaMundi;

    public AnagramaCount aC;

    public Sprite red, green, blue;
    // tempo
    
    public float tempoMin, tempoSeg;
    
    // Start is called before the first frame update
    void Start()
    {
        Pegatextos();
        LimpaTextos();
        separaPalavra(palavra);
        letras.Shuffle();
        txt.Shuffle();
        Letras();
        
    }

    // Update is called once per frame
    private void Update()
    {
        tempoSeg += Time.deltaTime;
        if (tempoSeg >= 60)
        {
            tempoMin++;
            tempoSeg = 0;
        }
        //Debug.Log("posInGame " + posInGame);
        if (acertos == certo.Count)
        {
            mapaMundi.SetActive(true);
            gameObject.SetActive(false);
            aC.acertos++;
        }
    }

    public void Pegatextos()
    {
        for (int i = 0; i < localTxt.transform.childCount; i++)
        {
            txt.Add(localTxt.transform.GetChild(i).GetComponentInChildren<Text>());
            
        }
    }

    public void LimpaTextos()
    {
        for (int i = 0; i < places.Count; i++)
        {
            //txt[i].text = " ";
            //txt[i].transform.parent.gameObject.SetActive(false);
            places[i].transform.parent.gameObject.SetActive(false);
        }
        
    }

    public void separaPalavra(string palavra)
    {
        foreach (char VARIABLE in palavra.ToCharArray())
        {
            //txt[pos].text += VARIABLE;
            txt[pos].transform.parent.gameObject.SetActive(true);
            
            letras.Add(VARIABLE.ToString());
            certo.Add(VARIABLE.ToString());
            pos++;
        }
        
    }

    public void Letras()
    {
        for (int i = 0; i < letras.Count; i++)
        {
            txt[i].text = letras[i];
            places[i].transform.parent.gameObject.SetActive(true);
        }
    }

    public void getWord()
    {
        if (posInGame <= certo.Count)
        {
        Text texto = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>();
        EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        places[posInGame].GetComponent<Text>().text = texto.text;
        //texto.text = null;
        //texto.gameObject.SetActive(false);
        CompareListas();

        posInGame++;
        //Debug.Log("posInGame " + posInGame);
        //Debug.Log(texto.name);

        }
    }

    public void ClearGame()
    {
        //posInGame = 0;
        for (int j = 0; j < places.Count; j++)
        {
            places[j].GetComponent<Text>().text = " ";
            if(places[j].activeInHierarchy) places[j].GetComponentInParent<Image>().sprite = blue;
            //localTxt.transform.GetChild(j).GetComponentInChildren<GameObject>().SetActive(true);
            // places[j].GetComponent<Button>().interactable = true;
        }

        for (int n = 0; n < localTxt.transform.childCount; n++)
        {
            localTxt.transform.GetChild(n).GetComponent<Button>().interactable = true;
        }

        posInGame = 0;
        acertos = 0;
    }

    public void CompareListas()
    {
        if (certo[posInGame] == places[posInGame].GetComponent<Text>().text)
        {
            places[posInGame].GetComponentInParent<Image>().sprite = green;
            acertos++;
        }
        else
        {
            places[posInGame].GetComponentInParent<Image>().sprite = red;
        }
    }

    public void ReGame()
    {
        
    }
    
    
}
