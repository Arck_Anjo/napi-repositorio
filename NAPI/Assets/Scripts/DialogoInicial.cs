using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogoInicial : MonoBehaviour
{
    //public TextMeshProUGUI dialogueText;
    public Text dialogueText;
    [TextArea]
    public string[] sentences;
    public int index=0;
    public float dialogue_speed;
    public float normal_speed;
    public bool nextText = true;
    public bool nomeJogadorBool;
    public GameObject nomeJogador;
    public Text txt;
    /*public GameObject boyOrGirlChoose;
    public bool menino, menina;*/
    public string nomeDoPersonagem;
    public bool isAuto = false;
    public GameObject chatBox;
    public bool acao;
    public bool stopLetering;
    //public GameObject shopBox;
    public GameObject mundo;
    public GameObject inicio;
    //nome do personagem que esta falando
    public GameObject local_de_fala;
    public string nome_do_jogador = "Jogador";
    public string nome_do_personagem;
    public GameObject personagem;
    public Color enable, disable;
    public List<Sprite> emocoes;
    public GameObject selecionarJogador;
    public bool aparencia;
    public GameObject posJogador;
    public GameObject localDeFalaPai;
    public GameObject setaDesafiante, setaJogador;

    //Verify if the player is closer to the NPC

    void Start()
    {
        normal_speed = dialogue_speed;
        nomeJogador.gameObject.SetActive(false);
        local_de_fala.GetComponent<Text>().text = nome_do_personagem;
        posJogador.SetActive(false);
        localDeFalaPai.SetActive(false);
        ChatBox();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            //isTrigger=false;
            //shopButton.SetActive(false);
            //chatBox.SetActive(false);
            //shopBox.SetActive(false);
        }
    }

    //NPC Chatbox

    void NextSentence()
    {
        if(index <= sentences.Length - 1)
        {
            CadaDialogo();
            /*dialogueText.text = "";
            StartCoroutine(WriteSentence());*/
        }
        else
        {
            StartMundo();
            Auto();
        }
    }
    
    

    IEnumerator WriteSentence()
    {
        foreach(char character in sentences[index].ToCharArray())
        {
            if (!stopLetering)
            {
                dialogueText.text += character;
                yield return new WaitForSeconds(dialogue_speed);
            }
        }
        index++;
        nextText=true;
        if (isAuto)
        {
            ChatBox();
        }
    }

    public void ChatBox()
    {
        chatBox.SetActive(true);
        if(nextText)
        {
            NextSentence();
            nextText=false;
        }
    }

    public void ResetIndex()
    {
        index = 0;
        nextText=true;
    }

    public void Instantantio()
    {
        dialogue_speed -= dialogue_speed;
    }

    public void NormalSpeed()
    {
        dialogue_speed = normal_speed;
    }

    public void StartMundo()
    {
        mundo.SetActive(true);
        inicio.SetActive(false);
    }

    void CadaDialogo()
    {
        switch (index)
        {
            case 0:
                dialogueText.text = "";
                StartCoroutine(WriteSentence());
                personagem.GetComponent<Image>().sprite = emocoes[5];
                setaJogador.SetActive(false);
                setaDesafiante.SetActive(true);
                break;
            case 1:
                dialogueText.text = "";
                StartCoroutine(WriteSentence());
                personagem.GetComponent<Image>().sprite = emocoes[0];
                break;
            case 2:
                if(!nomeJogadorBool)nomeJogador.SetActive(true);
                break;
            case 3:
                if(!aparencia)selecionarJogador.SetActive(true);
                break;
            case 4:
                posJogador.SetActive(true);
                posJogador.GetComponent<LocalJogador>().SetColor(0);
                local_de_fala.SetActive(true);
                localDeFalaPai.SetActive(true);
                dialogueText.text = "";
                StartCoroutine(WriteSentence());
                break;
            case 5:
                dialogueText.text = "";
                StartCoroutine(WriteSentence());
                personagem.GetComponent<Image>().sprite = emocoes[0];
                break;    
            case 6:
                nome_do_jogador = PlayerPrefs.GetString("Nome");
                local_de_fala.GetComponent<Text>().text = nome_do_jogador;
                posJogador.GetComponent<LocalJogador>().SetColor(1);
                posJogador.GetComponent<LocalJogador>().SetTriste();
                personagem.GetComponent<Image>().color = disable;
                setaJogador.SetActive(true);
                setaDesafiante.SetActive(false);
                dialogueText.text = "";
                StartCoroutine(WriteSentence());
                break;
            default:
                dialogueText.text = "";
                local_de_fala.GetComponent<Text>().text = nome_do_personagem;
                posJogador.GetComponent<LocalJogador>().SetColor(0);
                setaJogador.SetActive(false);
                setaDesafiante.SetActive(true);
                personagem.GetComponent<Image>().color = enable;
                StartCoroutine(WriteSentence());
                personagem.GetComponent<Image>().sprite = emocoes[3];
                break;
        }
        
        
    }

    public void SetName()
    {
        if (txt.text != String.Empty)
        {
            nomeDoPersonagem = nomeJogador.GetComponent<InputField>().text;
            nomeJogador.SetActive(false);
            nomeJogadorBool = true;
            PlayerPrefs.SetString("Nome",nomeDoPersonagem);
            PlayerPrefs.SetInt("NomeDoJogador", 1);
            PlayerPrefs.Save();
            stopLetering = false;
            dialogueText.text = "";
            StartCoroutine(WriteSentence());
            acao = true;
        }
    }

    /*public void SetGirl()
    {
        menina = true;
        boyOrGirlChoose.SetActive(false);
        stopLetering = false;
        dialogueText.text = "";
        StartCoroutine(WriteSentence());
        acao = true;
    }

    public void SetBoy()
    {
        menino = true;
        boyOrGirlChoose.SetActive(false);
        stopLetering = false;
        dialogueText.text = "";
        StartCoroutine(WriteSentence());
        acao = true;
    }*/

    public void Skip()
    {
        if (acao)
        {
            if (index < 1)
            {
                dialogueText.text = sentences[1];
                stopLetering = true;
                index = 1;
                nomeJogador.SetActive(true);
                acao = false;
                //dialogueText.text = "";
            }
            else if (index < 2 && nomeJogadorBool)
            {
                dialogueText.text = sentences[2];
                stopLetering = true;
                //index = 2;
                selecionarJogador.SetActive(true);
                acao = false;
            }
            else if (nomeJogador && aparencia)
            {
                index = sentences.Length;
                StartMundo();
            }
        }
    }
    
    public void Auto()
    {
        isAuto = !isAuto;
        Instantantio();
    }

    public void Seta()
    {
        if (!nextText)
        {
            //stopLetering = true;
            dialogue_speed = 0;
            return;
        }
        else if(nextText)
        {
            NormalSpeed();
            ChatBox();
            return;
        }
        
    }
    
    public void GotoMenu()
    {
        ResetIndex();
        SceneManager.LoadScene(0);
    }

    public void SetarAparencia()
    {
        aparencia = true;
        stopLetering = false;
        dialogueText.text = "";
        StartCoroutine(WriteSentence());
        acao = true;
    }
    
}
