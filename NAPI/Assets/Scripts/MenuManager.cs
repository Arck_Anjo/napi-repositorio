using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt("NomeDoJogador", 0);
        PlayerPrefs.SetInt("Desafio1Complete", 0);
        PlayerPrefs.SetInt("Desafio2Complete",0);
        PlayerPrefs.SetInt("Desafio3Complete",0);
        PlayerPrefs.SetInt("StartGame",0);
    }

    public void Sair()
    {
        Application.Quit();
    }
    
    
}
