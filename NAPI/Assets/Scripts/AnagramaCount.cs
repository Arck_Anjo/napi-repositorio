using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AnagramaCount : MonoBehaviour
{
    public int acertos, qtdMaximadeAcertos;
    public GameManager gm;
    [TextArea]
    public string[] falasVitoria;
    [TextArea]
    public string[] falasDerrota;
    public Text dialogueText;
    public int escolha;
    public float dialogue_speed;
    public float normal_speed;
    public GameObject desafioanteGame, desafiantePosGame;
    public Sprite win, lose;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Resultado()
    {
        if (acertos >= (qtdMaximadeAcertos/ 2))
        {
            Vitoria();
        }
        else
        {
            Derrota();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (acertos >= ((int)qtdMaximadeAcertos/2))
        {
            //gm.desafio3 = true;
            PlayerPrefs.SetInt("Desafio3Complete", 1);
        }
    }
    
    void Vitoria()
    {
        PlayerPrefs.SetInt("Desafio3Complete", 1);
        //desafioanteGame.SetActive(false);
        desafiantePosGame.SetActive(true);
        dialogueText.text = "";
        escolha = Random.Range(0, falasVitoria.Length);
        desafioanteGame.GetComponent<Image>().sprite = lose;
        StartCoroutine(WriteSentence(falasVitoria[escolha]));
    }

    void Derrota()
    {
        //desafioanteGame.SetActive(false);
        desafiantePosGame.SetActive(true);
        dialogueText.text = "";
        escolha = Random.Range(0, falasVitoria.Length);
        desafioanteGame.GetComponent<Image>().sprite = win;
        StartCoroutine(WriteSentence(falasDerrota[escolha]));
        //contagem = 0;
        acertos = 0;
    }
    
    IEnumerator WriteSentence(string resultado)
    {
        foreach(char character in resultado.ToCharArray())
        {
            dialogueText.text += character;
            yield return new WaitForSeconds(dialogue_speed);
        }
        
        yield return new WaitForSeconds(3f);
        End();
        //index++;
        //nextText=true;
    }

    public void End()
    {
        /*mapaMundi.SetActive(true);
        gameObject.SetActive(false);*/
        SceneManager.LoadScene(1);
    }
    public void Instantantio()
    {
        dialogue_speed -= dialogue_speed;
    }

    public void NormalSpeed()
    {
        dialogue_speed = normal_speed;
    }

    public void Skip()
    {
        End();
    }
}
