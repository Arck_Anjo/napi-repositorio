using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Trunfo")]
    public List<GameObject> trunfo;
    [Header("Quiz")]
    public GameObject quiz;
    [Header("Pontos")]
    public int pontos;

    [Header("Desafios")] public bool startGame;
    public GameObject desafio1M, desafio2M, desafio3M;
    public GameObject localDesafio1, localDesafio2, localDesafio3;
    public bool desafio1, desafio2, desafio3;
    public bool namePlayer;

    public GameObject inicialScene, mapaMundo;
    [Header("SaveFile")]
    private int pName, d_1, d_2, d_3, s_game;

   //private FMOD.Studio.VCA vca;
    public string vca_name;
    public Slider sound_slider;
    private void Start()
    {
        GetSave();
        if (pName != 1) return;
        inicialScene.SetActive(false);
        mapaMundo.SetActive(true);
        sound_slider = GetComponent<Slider>();
        //vca= FMODUnity.RuntimeManager.GetVCA("vca:/" + vca_name);
    }

    public void SetVolume(float volume)
    {
        //vca.setVolume(volume);
    }

    public void StartTrunfo()
    {
        var contagem = 0;
        
        trunfo[contagem].SetActive(true);
    }

    private void Update()
    {
        if (startGame)
        {
            localDesafio1.SetActive(true);
            localDesafio2.SetActive(true);
            localDesafio3.SetActive(true);
        }
        
        if (desafio3)
        {
            desafio3M.SetActive(true);
        }
    }

    public void GetSave()
    {
        pName = PlayerPrefs.GetInt("NomeDoJogador");
        d_1 = PlayerPrefs.GetInt("Desafio1Complete");
        d_2 = PlayerPrefs.GetInt("Desafio2Complete");
        d_3 = PlayerPrefs.GetInt("Desafio3Complete");
        s_game = PlayerPrefs.GetInt("StartGame");
        if (s_game == 1)
        {
            startGame = true;
        }
        if (d_1 == 1)
        {
            desafio1 = true;
        }
        if (d_2 == 1)
        {
            desafio2 = true;
        }
        if (d_3 == 1)
        {
            desafio3 = true;
        }
    }
}
